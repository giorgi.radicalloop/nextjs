import Styles from "./rightTitle.module.css"

const RightTitle = ({title}) => {
    return (
        <div className={Styles.title}>
            {title}
        </div>
    );
};

export default RightTitle;