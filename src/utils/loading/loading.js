import React from 'react';
import {Bars} from "react-loader-spinner";
const Loading = () => {
    return (
        <div className="vh-100 d-flex justify-content-center mt-5 ">
            <Bars
                height="80"
                width="80"
                radius="9"
                color="green"
                ariaLabel="loading"
                wrapperStyle
                wrapperClass
            />
        </div>
    );
};

export default Loading;