export const handleScroll = ({setPostsLength}) => {
    if (window.innerHeight + document.documentElement.scrollTop + 1 >= document.documentElement.scrollHeight) {
        setPostsLength((prev) => prev + 5)
    }
}