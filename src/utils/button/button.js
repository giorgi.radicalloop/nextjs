import Styles from "./button.module.css"

const Btn = ({value, bg, color, space }) => {
    return (
        <button className={`${Styles.btn} `} style={{backgroundColor: bg, color: color, marginTop: space}}>{value}</button>
    );
};

export default Btn;