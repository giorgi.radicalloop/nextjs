import Styles from "./sort.module.css"
import Select from 'react-select';
import {FaHotjar} from "react-icons/fa";
import {BiSun} from "react-icons/bi";
import {GoArrowUp} from "react-icons/go";
import {BsThreeDots, BsCardChecklist} from "react-icons/bs";
import {RiBankCardLine} from "react-icons/ri";
import {MdOutlineStorage} from "react-icons/md";
const Sort = () => {
    const options = [
        {
            value: "card",
            label: (
                <div>
                    <RiBankCardLine />
                </div>
            )
        },
        {
            value: "classic",
            label: (
                <div>
                    <BsCardChecklist />
                </div>
            )
        },
        {
            value: "compact",
            label: (
                <div>
                    <MdOutlineStorage />
                </div>
            )
        }
    ];
    return (
        <div className="row content d-flex">
            <div className={` ${Styles.sortOptions} d-flex  col-md-6 col-lg-6`}>
                <div className={Styles.activeSort}><FaHotjar size={20}/>Hot</div>
                <div><BiSun size={20}/>Sun</div>
                <div><GoArrowUp size={20}/>Top</div>
                <div><BsThreeDots size={20}/></div>
            </div>
            <div className="col-md-6 col-lg-6  d-flex justify-content-end align-items-center">
                <Select defaultValue={options[0].value} placeholder={options[0].label} isSearchable={false}  options={options}/>
            </div>
        </div>
    );
};

export default Sort;