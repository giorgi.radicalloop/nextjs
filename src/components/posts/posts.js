import Sort from "../sort/sort";
import Post from "../post/post";
import {useState} from "react";
const Posts = ({posts}) => {
    const [postsLength, setPostsLength] = useState(5)

    return (

        <div className="col-md-8 col-lg-8">
            <Sort/>
            <Post  posts={posts} />
        </div>
    );
};

export default Posts;