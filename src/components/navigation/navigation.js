import Styles from "./nav.module.css"

const Navigation = () => {
    return (
        <>
            <div className={`position-relative ${Styles.blackBackground}`}/>
            <div className={Styles.nav_parent}>
                <div className={`d-flex align-items-center  position-absolute ${Styles.logoSection}`}>
                    <div>
                        <img className={Styles.logoImage} alt="logo"
                             src="https://styles.redditmedia.com/t5_3h7yi/styles/communityIcon_nsrozhr9igl91.png?width=256&s=f48c940b5231ceb6cb810b8bb0bcf503395839bc"/>
                    </div>
                    <div className={Styles.titleParent}>
                        <div className={Styles.title}>
                            <div className="fw-bold" style={{fontSize: "28px"}}>Next.Js</div>
                            <div style={{color: "#7c7c7c"}}>r/nextjs</div>
                        </div>
                        <div className="mt-1">
                            <button className={Styles.joinButton}>Join</button>
                        </div>
                    </div>

                </div>
                <div className={`d-flex align-items-end h-100 ${Styles.nav_links}`}>
                    <div className={Styles.activeNav}>Posts</div>
                    <div>Website</div>
                    <select style={{border: "none"}}>
                        <option >Github</option>
                        <option>Code</option>
                    </select>
                </div>
            </div>
        </>
    );
};

export default Navigation;