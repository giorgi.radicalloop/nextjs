import React from 'react';
import RightTitle from "../../utils/right-title/rightTitle";
import {BsCardText} from "react-icons/bs";
import {GoPrimitiveDot} from "react-icons/go";
import Styles from "./rightSection.module.css"
import Btn from "../../utils/button/button";
const RightSection = () => {
    return (
        <div className="col-md-4 col-lg-4 ">
            <div>
                <RightTitle title="About community"/>
                <div className={Styles.rightInfo}>
                    <div >
                        <div  className={Styles.aboutInfo}>Next.js is the React framework for production by Vercel.</div>
                        <div  className={Styles.calendar}><BsCardText size={20}/> <span>Created Oct 25, 2016 </span></div>
                    </div>
                    <div className="d-flex mt-3">
                        <div >
                            <strong>21.8K</strong>
                            <h6 className={Styles.ab}>members</h6>
                        </div>
                        <div style={{marginLeft: "40px"}}>
                            <div><GoPrimitiveDot className="fw-bold" color="#46d160" />73</div>
                            <h6 className={Styles.ab}>Online</h6>
                        </div>
                    </div>
                </div>

            </div>
            <div>
                <RightTitle title="filter by flair"/>
                <div className={Styles.rightInfo}>
                    <Btn value="News" bg="#373c3f" color="#ffffff" />
                    <Btn value="Resource" bg="#ffd635" color="#000000"/>
                    <Btn value="Need help" bg="rgb(184, 0, 31)" color="#ffffff" />
                    <Btn space="10px" value="Discussion" bg="rgb(148, 224, 68)" color="#000000"/>
                </div>

                </div>
        </div>
    );
};

export default RightSection;