import axios from "axios";
import {useEffect, useState} from "react";
import Styles from "./post.module.css"
import {FaRegCommentAlt, FaShareSquare} from "react-icons/fa";
import {MdSaveAlt} from "react-icons/md";
import {ImArrowUp, ImArrowDown} from "react-icons/im";
import {FiExternalLink} from "react-icons/fi";
import Loading from "../../utils/loading/loading";

const Post = ({posts}) => {
    const [postsLength, setPostsLength] = useState(5)

    const handleScroll = () => {
        if (window.innerHeight + document.documentElement.scrollTop + 1 >= document.documentElement.scrollHeight) {
            setPostsLength((prev) => prev + 5)
        }
    }
    useEffect(() => {
        window.addEventListener("scroll", handleScroll)
    })

    return (posts === null ?<Loading/> : posts.slice(0, postsLength).map((post) => {
            const {data} = post
            const {pinned, author_fullname, title, num_comments, ups, selftext, domain} = data
            return (<div className={Styles.post}>
                    <div className="d-flex justify-content-center align-items-center ">
                        <div className="d-flex flex-column justify-content-center align-items-center">
                            <ImArrowUp/>
                            <span style={{margin: "5px 0 5px 0"}}>{ups}</span>
                            <ImArrowDown/>
                        </div>
                    </div>
                    <div className={Styles.postContent}>
                        {pinned && <h6 className="small">pinned by ${author_fullname}</h6>}
                        <h6 className="small">Posted By ${author_fullname} </h6>
                        <h5 style={{color: "#1c1c1c"}}>{title}</h5>
                        {selftext ? <p className={Styles.selfText}>{selftext.substring(0,300)}...</p> :
                            <p>{domain} <FiExternalLink/></p>}
                        <div className="d-flex small">
                            <div>
                                <FaRegCommentAlt/> {num_comments} <span>Comments</span>
                            </div>
                            <div style={{marginLeft: "15px"}}>
                                <FaShareSquare/> <span>Share</span>
                            </div>
                            <div style={{marginLeft: "15px"}}>
                                <MdSaveAlt/> <span>Save</span>
                            </div>
                        </div>
                    </div>
                </div>)
        }));
};

export default Post;