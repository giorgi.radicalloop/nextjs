import Header from "./components/header/header";
import Navigation from "./components/navigation/navigation";
import Posts from "./components/posts/posts";
import RightSection from "./components/right-section/rightSection";
import {useEffect, useState} from "react";
import axios from "axios";
import Loading from "./utils/loading/loading";

const App = () => {

    const [posts, setPosts] = useState(null)


    useEffect(() => {
        axios.get('https://www.reddit.com/r/nextjs.json').then((res) => {
            const data = res.data.data.children
            setPosts(data)
        }).catch((err) => console.log(err))
    }, [])
    return (
        <>
            <Header/>
            <Navigation/>
            {posts === null ? <Loading /> :
            <div className="parent row ">
                <Posts posts={posts} />
                <RightSection />
            </div>}
        </>
    );
};

export default App;